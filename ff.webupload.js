/**
 * 上传组件
 * by fang.j
 */


/**
 * 	                // 图片预览 function 2
	                // 下面是关键的关键，通过这个 file 对象生成一个可用的图像 URL  
	                // 获取 window 的 URL 工具  
	                // var URL = window.URL || window['webkitURL'];  
	                // 通过 file 生成目标 url  
	                // var imgURL = URL.createObjectURL(file);  
	                // 用这个 URL 产生一个 <img> 将其显示出来  
	                // demand.image = imgURL;  
	                // 使用下面这句可以在内存中释放对此 url 的伺服，跑了之后那个 URL 就无效了  
	                // URL.revokeObjectURL(imgURL);  
	  
	  
	                 // that.fileUpload(file);//预览出现后调用上传方法，传值为一个file对象，也可以传demand.image预览地址，看服务器那边怎么要求的  
 					//需要读取进度时启用下面的方法
		            //该事件不支持跨域！
//		            uploadProgress: function(event, position, total, percentComplete) {
//				       console.log(percentComplete);
//				    },
//		            

//					data: {"type":"xxx"},
//					forceSync: true,
//					//提交表单之前做的验证
//		            beforeSubmit:function(){
//		               
//		
//		            },
 * 
 * 
 * */
	var FFUpload = function(){};
	
	FFUpload.prototype={
		
		server:"",
		//表单插入的位置
		picker:"",
		cPicker:"",
		//需要发送到服务端的表单数据
		formData:[],
		//表单中的上传域
		fileDomainId:"",
		//表单ID
		formId:"",
		//表单对象
		cForm:null,
		//文件域的绑定
		cFileInput:null,
		//上传按钮
		actButId:"",
		//上传按钮对象
		cActBut:null,
		/*缩略图*/
		thumbnail:null,
		/*自动上传*/
		autoUpload:false,
		/*有效性验证*/
		valid:{
			ext:['jpg'],
			size:0,
		},
		//验证失败的操作符
		validOp:{
			EXT:"EXT",
			SIZE:"SIZE",
		},
		/*上传状态*/
		uploadIng:false,
		/*当前正在操作的文件*/
		currActionFile:{},

		
		/*
		 * events
		 * fileChange 上传域文件发生变化
		 * upLoadSuccess 上传成功
		 * error 上传错误
		 * 
		 * */
		events:{},
		
		
		/*事件绑定*/
		on:function(event,callback){
			this.events[event] = callback;
		},
		
		getUUID:function(len, radix){
			
			
		    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split('');
		    var uuid = [], i;
		    radix = radix || chars.length;
		 
		    if (len) {
		      // Compact form
		      for (i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
		    } else {
		      // rfc4122, version 4 form
		      var r;
		 
		      // rfc4122 requires these characters
		      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
		      uuid[14] = '4';
		 
		      // Fill in random data.  At i==19 set the high bits of clock sequence as
		      // per rfc4122, sec. 4.1.5
		      for (i = 0; i < 36; i++) {
		        if (!uuid[i]) {
		          r = 0 | Math.random()*16;
		          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
		        }
		      }
		   
		 	}
		    return uuid.join('');
		},
			
		
		
		initOpt:function(opt){
			
			if( opt.hasOwnProperty("server") && opt.server!=""){
				this.server = opt.server;
			}else{
				console.log("need server addr!");
				return false;
			}
			
			if( opt.hasOwnProperty("formData") && opt.formData!=null && opt.formData.length>0){
				this.formData = opt.formData;
			}
			
			if( opt.hasOwnProperty("formId") && opt.formId!=""){
				this.formId = opt.formId;
			}else{
				this.formId = this.getUUID(8,10);
			}
			
			if( opt.hasOwnProperty("autoUpload")){
				this.autoUpload = opt.autoUpload;
			}
			if( opt.hasOwnProperty("fileDomainId") && opt.fileDomainId!=""){
				this.fileDomainId = opt.fileDomainId;
			}
			
			if( opt.hasOwnProperty("picker") && opt.picker!=""){
				this.picker = opt.picker;
			}
			
      		if( opt.hasOwnProperty("actButId") && opt.actButId!=""){
				this.actButId = opt.actButId;
			}else{
				this.actButId = this.getUUID(8,10);
			}
      		
      		if( opt.hasOwnProperty("valid")){
				this.valid.ext = !opt.valid.hasOwnProperty("ext") ? this.valid.ext : opt.valid.ext;
				this.valid.size = !opt.valid.hasOwnProperty("size") ? this.valid.size : opt.valid.size;
			}
      		return true;

		},
		
		initVal:function(){
			this.cPicker	= $("#"+this.picker);
			
		},
		
		//创建表单
		create:function(opt){
			
		
			if(!this.initOpt(opt)){
				return;
			}
			this.initVal();
			
			$('<form method="post" enctype="multipart/form-data"></form>').appendTo(this.cPicker);
			var str="";
            //创建表单数据
            for(var e = 0 ; e< this.formData.length;e++){
            	console.log(this.formData[e]);
            	str+='<input type=hidden name="'+this.formData[e].name+'" value="'+this.formData[e].val+'">';
            }
            //创建上传域
            str+='<a href="javascript:;" class="file">+<input id="'+this.fileDomainId+'" type=file name="'+this.fileDomainId+'" ></a>';
            
            			
			this.cForm = this.cPicker.find("form");
			this.cForm.attr("id",this.formId);
            this.cForm.attr("action",this.server);
			this.cForm.append(str);
			this.cFileInput = $("#"+this.fileDomainId);
            this.bindEvt();
		},
	
		//执行上传
		upload:function(){
			var self = this;
			var options = {

					//服务器返回结果处理
		        success:function(data){
		        	self.uploadIng  = false;
		            self.events["upLoadSuccess"](data,self.currActionFile);
		        },

	           	complete: function(xhr) {
	           		
	           		self.uploadIng  = false;
	           		self.cFileInput.val("");
	           		
			        if (xhr.statusText == "OK") {
			        	
	            		
			        }else {
			        	self.events["error"](xhr,self.currActionFile);
						return false;
			        }
		    	}
		
		        };
				self.uploadIng  = true;
				self.cForm.ajaxSubmit(options);

		        return false;
		},
		
		/*绑定事件*/
		bindEvt:function(){
			
			var self = this;
		
			this.cFileInput.on('change', function (event) {  
				
	            // 根据这个 <input> 获取文件的 HTML5 js 对象  
	            var files = event.target.files, 
	            	file,
	            	vSize=false,
	            	vData=false,
	            	data=null,
	            	vExt =false;
	            	
	            if (files && files.length > 0) {  
	                // 获取目前上传的文件  
	                file = files[0];  
	     			
	     			vSize = file.size > self.valid.size;
	     			
	     
	              
	  				//验证格式
					var i1=file.name.lastIndexOf("."),
						i2=file.name.length,
						ext = file.name.substring(i1+1,i2);//后缀名
						
					self.currActionFile = file;
					self.currActionFile["ext"]  = ext;
					self.currActionFile["id"]   = self.getUUID(8,10);
					
					
					vExt = $.inArray( self.currActionFile.ext, self.valid.ext ) < 0 ;
					
	  			
	  
	  				if (vSize) {  
	                   self.events["validFail"](self.currActionFile,self.validOp.SIZE);
	  				   return;
	                } 
	                
	                if (vExt) {  
	                   self.events["validFail"]( self.currActionFile, self.validOp.EXT );
	  				   return;
	                }  
	  
	  				var reader = window['FileReader'] ? new FileReader() : null;
	              

	                var imageType = /^image\//;  

					reader.readAsDataURL(file);
					
	                //读取完成  
	                reader.onload = function (e) {  
	                	self.currActionFile["data"] = e.target.result;
	              		self.events["fileChange"](self.currActionFile);
	                };  
	                
	               
	                

	            }  
       	 	});  
			
			
		},
		
		
		
	};
 